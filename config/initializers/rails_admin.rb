RailsAdmin.config do |config|

  ### Popular gems integration

  ## == Devise ==
   config.authenticate_with do
     warden.authenticate! scope: :user
   end
   config.current_user_method(&:current_user)

  ## == Cancan ==
   config.authorize_with :cancan

  ## == Pundit ==
  # config.authorize_with :pundit

  ## == PaperTrail ==
  # config.audit_with :paper_trail, 'User', 'PaperTrail::Version' # PaperTrail >= 3.0.0

  ### More at https://github.com/sferik/rails_admin/wiki/Base-configuration

  module RailsAdmin
          module Config
              module Actions
                  class Sendnotif < RailsAdmin::Config::Actions::Base
                      RailsAdmin::Config::Actions.register(self)

                      register_instance_option :visible? do
                          true
                      end

                      register_instance_option :member do
                          true
                      end

                      register_instance_option :link_icon do
                          #FontAwesome Icons
                          'icon-envelope'
                      end

                      register_instance_option :controller do
                            Proc.new do

                              user = User.find(params[:id])
                              user_points = user.total_points
                              user_email = current_user.email

                              SendnotifMailer.sendnotif_email(user).deliver

                              flash[:success] = "Notification has been successfully sent."
                              redirect_to back_or_index

                            end
                      end
                  end
              end
          end
      end
      

  config.actions do
    dashboard                     # mandatory
    index                         # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    sendnotif do
  # Make it visible only for article model. You can remove this if you don't need.
      visible do
          bindings[:abstract_model].model.to_s == "User"
      end

    end
    
  end
end
