Rails.application.routes.draw do
  devise_for :users
  mount RailsAdmin::Engine => '/admin', as: 'rails_admin'

  namespace :api do
    namespace :v1 do

    devise_scope :user do
      post 'users/sign_up' => 'registrations#create', :as => 'user_create'
      post 'users/sign_in' => 'sessions#create', :as => 'login'
      delete 'users/sign_out' => 'sessions#destroy', :as => 'logout'
    end

      # User Resources
      get '/users' => 'users#index'
      get '/user' => 'users#show_user'
      put '/users/update_signup' => 'registrations#update_signup'
      #put '/users/update_photo' => 'users#update_photo'
      put '/users/update_total_points' => 'users#update_total_points'
      put '/users/update_unionbank_accnt_num' => 'users#update_unionbank_accnt_num'


      # Credit Card
      post '/credit_cards' => 'credit_cards#create'

      # Payment Gateway
      post '/payment_gateways' => 'payment_gateways#create'
      put '/payment_gateways/update_image' => 'payment_gateways#update_image'

    end
  end

end
