class CreatePaymentGateways < ActiveRecord::Migration
  def change
    create_table :payment_gateways do |t|
    	t.string :reference_number
    	t.date :statement_date
    	t.date :due_date
    	t.date :date_of_payment
    	t.string :biller
    	t.float :amount
    	t.string :image
      t.timestamps null: false
    end
  end
end
