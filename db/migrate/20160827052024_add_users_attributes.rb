class AddUsersAttributes < ActiveRecord::Migration
  def change
  	add_column :users, :user_name, :string
  	add_column :users, :first_name, :string
  	add_column :users, :middle_name, :string
  	add_column :users, :last_name, :string
  	add_column :users, :birthday, :date
  	add_column :users, :age, :integer
  	add_column :users, :address, :string
  	add_column :users, :id_number, :string
  	add_column :users, :id_type, :string
  	add_column :users, :mobile_number, :string
  	
  end
end
