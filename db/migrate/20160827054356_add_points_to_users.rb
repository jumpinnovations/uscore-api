class AddPointsToUsers < ActiveRecord::Migration
  def change
  	add_column :users, :payment_gateway_points, :float
  	add_column :users, :credit_card_points, :float
  	add_column :users, :total_points, :float
  end
end
