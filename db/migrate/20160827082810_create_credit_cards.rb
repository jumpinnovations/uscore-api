class CreateCreditCards < ActiveRecord::Migration
  def change
    create_table :credit_cards do |t|
      t.string :accountNum
      t.string :cardNumber
      t.string :cardBrand
      t.string :cardType
      t.string :CardCCV
      t.string :nameOnCard
      t.string :cardExpiryMonth
      t.string :cardExpiryYear
      t.string :currency
      t.float :amount	

      t.timestamps null: false
    end
  end
end
