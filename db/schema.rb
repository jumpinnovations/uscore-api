# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160828000121) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "credit_cards", force: :cascade do |t|
    t.string   "accountNum"
    t.string   "cardNumber"
    t.string   "cardBrand"
    t.string   "cardType"
    t.string   "CardCCV"
    t.string   "nameOnCard"
    t.string   "cardExpiryMonth"
    t.string   "cardExpiryYear"
    t.string   "currency"
    t.float    "amount"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "user_id"
  end

  create_table "payment_gateways", force: :cascade do |t|
    t.string   "reference_number"
    t.date     "statement_date"
    t.date     "due_date"
    t.date     "date_of_payment"
    t.string   "biller"
    t.float    "amount"
    t.string   "image"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "user_id"
    t.string   "payment_center"
    t.string   "photo_url"
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "user_name"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "last_name"
    t.date     "birthday"
    t.integer  "age"
    t.string   "address"
    t.string   "id_number"
    t.string   "id_type"
    t.string   "mobile_number"
    t.string   "role"
    t.float    "payment_gateway_points"
    t.float    "credit_card_points"
    t.float    "total_points"
    t.string   "token"
    t.string   "auth_token"
    t.string   "device_token"
    t.string   "gender"
    t.integer  "user_id"
    t.string   "unionbank_account_num"
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

end
