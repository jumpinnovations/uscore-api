class PaymentGateway < ActiveRecord::Base
	belongs_to :users

	mount_uploader :image, ImageUploader

	def biller_enum
		[['MERALCO'], ['PLDT'], ['HOUSE RENT'], ['PAG-IBIG LOAN'], ['SMART'], ['GLOBE'], ['MAXICARE'], ['INTELLICARE']]
	end

	def payment_center_enum
		[['BAYAD CENTER'],['BDO ONLINE'], ['SM Bills Payment Center']]
	end

	RailsAdmin.config do |config|
		config.model 'PaymentGateway' do

			create do
				field :biller
				field :payment_center
				field :amount
				field :reference_number
				field :image
				field :due_date
				field :date_of_payment
				field :statement_date
				field :user_id
			end
			
			list do
				field :biller
				field :payment_center
				field :amount
				field :reference_number
				field :image
				field :due_date
				field :date_of_payment
				field :statement_date
				field :user_id
			end

			edit do
				field :biller
				field :payment_center 
				field :amount
				field :reference_number
				field :image
				field :due_date
				field :date_of_payment
				field :statement_date
				field :user_id
			end
		end
	end 

end
