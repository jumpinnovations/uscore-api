class Ability
  include CanCan::Ability

  def initialize(user)
    
   if user.role == "admin"
      can :access, :rails_admin
      can :manage, :all  

    elsif user.role == "guest"
       can :read, :all
    end

  end
end
