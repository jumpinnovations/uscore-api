class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :payment_gateway

  has_many :credit_card


   def id_type_enum
    [['N/A'],['PASSPORT'], ['GSIS'], ['LICENSE'], ['SSS'], ['PHILHEALTH'], ['PRC']]
  end

  def payment_gateway_points_enum
    [['0'],['50'], ['100'], ['150'], ['200'], ['250'], ['300'], ['350'],['400']]
  end

  def credit_card_points_enum
    [['0'],['50'], ['100'], ['150'], ['200'], ['250'], ['300'], ['350'],['400']]
  end

  RailsAdmin.config do |config|
  	config.model 'User' do

  		create do 
	  	    field :email
	        field :first_name
	        field :last_name
	        field :id_type
	        field :id_number
	        field :address
	        field :payment_gateway_points
	        field :credit_card_points
	        field :total_points
	        field :password
  		end

  		edit do
  			field :email
	        field :user_name
	        field :payment_gateway_points
	        field :credit_card_points
          field :total_points
	        field :first_name
	        field :last_name
	        field :id_type
	        field :id_number
	        field :address
  		end

  		list do
  			field :email
	        field :first_name do
	            pretty_value do
	              full_name = bindings[:object]
	                %{<p>#{full_name.first_name} #{full_name.last_name}</p>}.html_safe
	            end
	            label "Full name"
	          end
	        field :payment_gateway_points
	        field :credit_card_points
	        field :total_points 

    			field :id_type
    			field :id_number
    			field :address
    			field :payment_gateway
	       
  		end

      show do

        field :email
        field :first_name do
            pretty_value do
              full_name = bindings[:object]
                %{<p>#{full_name.first_name} #{full_name.last_name}</p>}.html_safe
            end
            label "Full name"
          end
        field :payment_gateway_points
        field :credit_card_points
        field :total_points

        field :last_name
        field :id_type
        field :id_number
        field :address  
        field :payment_gateway 
   
      end
  	end
  end
end
