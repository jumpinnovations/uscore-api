class SendnotifMailer < ApplicationMailer
	default from: "claire.bayoda@jumpdigital.asia"

	def sendnotif_email(user)
		@user = user
		@url = "localhost:3000"
  		mail(to: @user.email, subject: 'UScore Email Notification')
	end

end
