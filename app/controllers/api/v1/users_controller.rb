class Api::V1::UsersController < ApplicationController
respond_to :json

  def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :error => [] }
  end

  # Listing of all Users
  def index
    if user = User.all
      @http_status = 200
      @http_response[:status] = true
      @http_response[:results] = user 
   
      render :json => @http_response.to_json, :status => @http_status
    end
  end

  # Show users profile by token login
  def show_user
  	if request.headers["x-auth-token"]
      user = User.find_by_token(request.headers["x-auth-token"])
    	if user.present?
        @http_status = 200
        @http_response[:status] = true
        @http_response[:results] = user
    		render :json => @http_response.to_json(:include => [:payment_gateway, :credit_card]), :status => @http_status

    	else
        @http_status = 400
        @http_response[:status] = false
        @http_response[:error_code] = '2'
        @http_response[:error_description] = "Authentication Error"
        @http_response[:error] = { :message => "Invalid user" }
    		render :json => @http_response.to_json,:status => @http_status
    	end
    else
      @http_status = 400
      @http_response[:error_code] = '20'
      @http_response[:error_description] = "Token Not Found"
      @http_response[:error] = { :message => "Invalid token" }
      @http_response[:status] = false
      render :json => @http_response.to_json,:status => @http_status
    end	
  	
  end

  #def update_photo
  #  if request.headers["x-auth-token"]
    #  user = User.find_by_token(request.headers["x-auth-token"])
     # return user_failure unless user

     # if params[:photo_url]
     #     user.photo_url = params[:photo_url]          
     # end
    #  
     # user.save
     # @http_status = 200
     # @http_response[:status] = true
     # @http_response[:result] = user

      #  render :json => @http_response.to_json, :status => @http_status
   # end
 # end

  def update_total_points
    if request.headers["x-auth-token"]
      user = User.find_by_token(request.headers["x-auth-token"])
      return user_failure unless user

      if user
          user_update = User.find(params[:id])
          user_update.id = params[:id] 
          user_update.total_points = params[:total_points]          
      
          if user_update.save
              @http_status = 200
              @http_response[:status] = true
              @http_response[:results] = user_update
              
            else
              validation_error(user_update) 
            end

        end
      else
        @http_status = 400
        @http_response[:error_code] = '20'
        @http_response[:error_description] = "Token Not Found"
        @http_response[:error] = [{ :message =>  "Invalid token" }]
        @http_response[:status] = false

        end
        
        render :json => @http_response.to_json,:status => @http_status
        
  end

  def update_unionbank_accnt_num
    if request.headers["x-auth-token"]
      user = User.find_by_token(request.headers["x-auth-token"])
      return user_failure unless user

      if user
          user_update = User.find(params[:id])
          user_update.id = params[:id] 
          user_update.unionbank_account_num = params[:unionbank_account_num]          
      
          if user_update.save
              @http_status = 200
              @http_response[:status] = true
              @http_response[:results] = user_update
              
            else
              validation_error(user_update) 
            end

        end
      else
        @http_status = 400
        @http_response[:error_code] = '20'
        @http_response[:error_description] = "Token Not Found"
        @http_response[:error] = [{ :message =>  "Invalid token" }]
        @http_response[:status] = false

        end
        
        render :json => @http_response.to_json,:status => @http_status
        
  end
  
  # Show user by ID
  #def show_user
   # user = User.find(params[:id])
    #respond_with user, serializer: Users::ShowSerializer
  #end

  # User deactivation profile
  def delete_user
	if params[:user_id].present?
	    user = User.destroy(params[:user_id])
	      @http_status = 200
	      @http_response[:status] = true
	      @http_response[:results] = { :message => 'Successfully Deleted' }
	      @http_response[:error] = []
	      render :json => @http_response.to_json,:status => @http_status
    else
	      @http_status = 400
	      @http_response[:status] = false
	      @http_response[:error_code] = '1'
	      @http_response[:error_description] = "Validation Error"
	      @http_response[:error] = { :message => "user_id required" }
	      render :json => @http_response.to_json,:status => @http_status
   	end

  end



end