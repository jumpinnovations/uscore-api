class Api::V1::CreditCardsController < ApplicationController
	
	respond_to :json

	def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :error => [] }
	end


	def create
		user = CreditCard.new
		user.user_id = params[:user_id]
		user.accountNum = params[:accountNum]
		user.cardNumber = params[:cardNumber]
		user.cardBrand = params[:cardBrand]
		user.cardType = params[:cardType]
		user.CardCCV = params[:CardCCV]
		user.nameOnCard = params[:nameOnCard]
		user.cardExpiryMonth = params[:cardExpiryMonth]
		user.cardExpiryYear = params[:cardExpiryYear]
		user.currency = params[:currency]
		user.amount = params[:amount]
	
	    user.save
          @http_status = 200
          @http_response[:status] = true
          @http_response[:results] = user

	    render :json => @http_response.to_json, :status => @http_status
	end


end