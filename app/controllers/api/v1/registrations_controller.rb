class Api::V1::RegistrationsController < ApplicationController
skip_before_filter :verify_authenticity_token, :if => Proc.new { |c| c.request.format == 'application/json' }

	respond_to :json, :xml

	def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :error => [] }
	end

	def index
		render :json => @http_response.to_json,:status => @http_status
	end

	def create
		user = User.new
		user.email = params[:email]
		user.mobile_number = params[:mobile_number]
		user.password = params[:password]
		user.first_name = params[:first_name]
		user.middle_name = params[:middle_name]
		user.birthday = params[:birthday]
		user.last_name = params[:last_name]
		user.user_name = params[:user_name]
		user.age = params[:age]
		user.gender = params[:gender]
		user.address = params[:address]
	    user.save
          @http_status = 200
          @http_response[:status] = true
          @http_response[:results] = user

	    render :json => @http_response.to_json, :status => @http_status
	end

	def update_signup
		if request.headers["x-auth-token"]
      	user = User.find_by_token(request.headers["x-auth-token"])
      	return user_failure unless user

      if user
        user_update = User.find(params[:id])
        user_update.id = params[:id]
        user_update.id_type = params[:id_type]
        user_update.id_number = params[:id_number]
      
        if user_update.save
          @http_status = 200
          @http_response[:status] = true
          @http_response[:results] = user_update
          
        else
          validation_error(user_update) 
        end

      end

    else
      @http_status = 400
      @http_response[:error_code] = '20'
      @http_response[:error_description] = "Token Not Found"
      @http_response[:error] = [{ :message =>  "Invalid token" }]
      @http_response[:status] = false
    end

    render :json => @http_response.to_json,:status => @http_status
	end


end