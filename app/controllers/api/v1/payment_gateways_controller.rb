class Api::V1::PaymentGatewaysController < ApplicationController
respond_to :json

	def initialize
		@http_status = 400
		@http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :error => [] }
	end


	def create
		user = PaymentGateway.new
		user.user_id = params[:user_id]
		user.reference_number = params[:reference_number]
		user.statement_date = params[:statement_date]
		user.date_of_payment = params[:date_of_payment]
		user.due_date = params[:due_date]
		user.amount = params[:amount]
		user.payment_center = params[:payment_center]
		user.biller = params[:biller]
	
	    user.save
          @http_status = 200
          @http_response[:status] = true
          @http_response[:results] = user

	    render :json => @http_response.to_json, :status => @http_status
	end

	def update_image
		if request.headers["x-auth-token"]
      		user = User.find_by_token(request.headers["x-auth-token"])
      		return user_failure unless user

	      	if user
	      		user_update_payment = PaymentGateway.find(params[:id])
	      		user_update_payment.id = params[:id]
			    user_update_payment.image = params[:image]         

	      		if user_update_payment.save
		          @http_status = 200
		          @http_response[:status] = true
		          @http_response[:results] = user_update_payment
		          
		        else
		          validation_error(user_update_payment) 
		        end

	      	end
	    else
	      @http_status = 400
	      @http_response[:error_code] = '20'
	      @http_response[:error_description] = "Token Not Found"
	      @http_response[:error] = [{ :message =>  "Invalid token" }]
	      @http_response[:status] = false

        end
        
        render :json => @http_response.to_json,:status => @http_status

	end

end