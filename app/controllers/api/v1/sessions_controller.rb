class Api::V1::SessionsController < ApplicationController

respond_to :json

  def initialize
    @http_status = 400
    @http_response = { :error_code => '0', :error_description => [] , :status => false , :results => [] , :error => [] }
  end

  def create
    resource = User.find_for_database_authentication(:user_name => params[:user_name])

    if resource.valid_password?(params[:password])
        resource.token = ensure_authentication_token
        resource.device_token = params[:device_token]
        
        if resource.save
          @http_status = 200
          @http_response[:status] = true
          @http_response[:results] = { :user_id => resource.id, :auth_token => resource.token, :user_name => resource.user_name }

        else
          validation_error(resource)
        end
    end

    render :json => @http_response, :status => @http_status

  end

  def destroy
    resource = User.find_by_token(params[:token]||request.headers["x-auth-token"])
    if resource.present?
      resource.token = nil
      resource.save
      @http_status = 200
      @http_response[:status] = true
      @http_response[:results] = { :message => "Logout"}
      
      render :json => @http_response, :status => @http_status
    end
  end

  def ensure_authentication_token
    token ||= generate_authentication_token
  end

private

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(token: token).first
    end

  end

end
