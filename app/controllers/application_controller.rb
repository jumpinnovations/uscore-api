class ApplicationController < ActionController::Base 
    # Prevent CSRF attacks by raising an exception. 
    # For APIs, you may want to use :null_session instead. 
    protect_from_forgery with: :exception 
    protect_from_forgery with: :null_session, :if => Proc.new { |c| c.request.format == 'application/json' }


    # API Validation
    def check_token
    	@user = User.find_by_token(request.headers["x-auth-token"]) && request.headers["x-auth-token"]
    	return user_failure unless @user 
    end

    def user_failure
	    @http_status = 400
	    @http_response[:status] = false
	    @http_response[:error_code] = '2'
	    @http_response[:error_description] = "Authentication Error"
	    @http_response[:error] =  [{ :message => "You must login first" }]
	    render :json => @http_response,:status => @http_status
	end

    def validation_error(error_model)
        @http_status = 400
        msg = error_model.errors.full_messages
        key = error_model.errors.keys
        errors ||= Array.new
        msg.each_with_index do |m, k|
          full_error = Hash.new(m)
          full_error = {key[k] => m}
          errors.push(full_error)
        end
        @http_response[:error_code] = '1'
        @http_response[:error_description] = "Validation Error"
        @http_response[:error] = errors
    end

    def no_pref
        @http_status = 400
        @http_response[:error_code] = '30'
        @http_response[:error_description] = "No Result"
        @http_response[:status] = false
        @http_response[:error] =  [{ :message => "No preferences" }]
          
        
      end
    
end